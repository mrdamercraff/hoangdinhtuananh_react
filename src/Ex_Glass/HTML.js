import React, { Component } from "react";

export default class HTML extends Component {
  state = {
    img: null,
  };
  handleChangeGlass = (id) => {
    this.setState({
      img: `./glassesImage/${id}.png`,
    });
  };
  render() {
    return (
      <div
        className="content
       "
      >
        <div className="header text-center text-white p-3">
          <h3>TRY GLASSES APP ONLINE</h3>
        </div>
        <div className=" m-3 d-flex justify-content-between">
          <div className="model col-4"></div>
          <div className="model1 col-4  ">
            <div className="vglasses__card">
              <div className="mb-2 text-right mt-2 mr-2"></div>
              <div className="vglasses__model" id="avatar">
                <img src={this.state.img} alt="" />
              </div>
              <div id="glassesInfo" className="vglasses__info"></div>
            </div>
          </div>
        </div>
        <div className="m-3 w-50 m-auto row glasses ">
          <img
            onClick={() => {
              this.handleChangeGlass("v1");
            }}
            className="col-3"
            src={"./glassesImage/g1.jpg"}
            alt=""
          />
          <img
            onClick={() => {
              this.handleChangeGlass("v2");
            }}
            className="col-3"
            src={"./glassesImage/g2.jpg"}
            alt=""
          />
          <img
            onClick={() => {
              this.handleChangeGlass("v3");
            }}
            className="col-3"
            src={"./glassesImage/g3.jpg"}
            alt=""
          />
          <img
            onClick={() => {
              this.handleChangeGlass("v4");
            }}
            className="col-3"
            src={"./glassesImage/g4.jpg"}
            alt=""
          />
          <img
            onClick={() => {
              this.handleChangeGlass("v5");
            }}
            className="col-3"
            src={"./glassesImage/g5.jpg"}
            alt=""
          />
          <img
            onClick={() => {
              this.handleChangeGlass("v6");
            }}
            className="col-3"
            src={"./glassesImage/g6.jpg"}
            alt=""
          />
          <img
            onClick={() => {
              this.handleChangeGlass("v7");
            }}
            className="col-3"
            src={"./glassesImage/g7.jpg"}
            alt=""
          />
          <img
            onClick={() => {
              this.handleChangeGlass("v8");
            }}
            className="col-3"
            src={"./glassesImage/g8.jpg"}
            alt=""
          />
          <img
            onClick={() => {
              this.handleChangeGlass("v9");
            }}
            className="col-3"
            src={"./glassesImage/g9.jpg"}
            alt=""
          />
        </div>
      </div>
    );
  }
}
